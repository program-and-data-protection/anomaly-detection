# Détection d'anomalie avec MATLAB

[Anomaly-detection](https://github.com/isabelleysseric/Anomaly-detection) (GitHub)
&nbsp; • &nbsp;[Anomaly-detection](https://github.com/isabelleysseric/Anomaly-detection/wiki) (Wiki)
&nbsp; • &nbsp;[isabelleysseric.com](https://isabelleysseric.com) (Portfolio)
&nbsp; • &nbsp;[isabelle-eysseric](https://www.linkedin.com/in/isabelle-eysseric/) (Linkedin)
&nbsp; • &nbsp;[isabelleysseric](https://hub.docker.com/u/isabelleysseric) (Docker)
<br/>
<br/>


<p align='center'>
  <img src="https://github.com/isabelleysseric/Anomaly-detection/blob/main/images/anomaly_detection_1.png" />
</p>
<br/>
<br/>


## Repertoire

Dans le dossier **code**, il y a un fichiers *anomaly_detection.m* avec l'execution de l'algorithme de détection d'anomalie.

Dans le dossier **data**, il y a l'image de la vertèbre utilisées pour la détection d'anomalie. 

Dans le dossier **images**, il y a les images utilisées dans le wiki pour visualiser les résultats. 
<br/>
<br/>


Anomaly-detection:

- **code**
  - *anomaly_detection.m*
  
- **data**
  - *vertebre.png*
  
- **images**
  - *anomaly_detection_1.png*
